// Store the house price variation
var evolucionPrecioVivienda = new Array(20);

// Inicializa la evolución del precio de la vivienda
evolucionPrecioVivienda =[-10,-6,-3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,2,2]

function updateEvolucionPrecioVivienda(anyo, precio){
    evolucionPrecioVivienda[anyo] = precio;
}

function calcula () {
	// All the time units are expressed in months (unless stated otherwise)
	var precioCasa = parseFloat($("#precioCasa").val());
	var loanInterest =  parseFloat($("#loanInterest").val());
	var plazo = parseFloat($("#term").val());
	var alquiler = parseFloat($("#alquiler").val());
	var gastosCompraPorc = parseFloat($("#gastosCompra").val());
	var capitalInicial = parseFloat($("#capitalInicial").val());
	var rendimientoCapital = parseFloat($("#rendimientoCapital").val());
    var ipc = parseFloat($("#ipc").val());


    // Try to maintain old values
    var aux = new Array(Math.floor(plazo/12));
    for(var i=0;i<aux.length;i++){
        if(i<evolucionPrecioVivienda.length){
            aux[i] = evolucionPrecioVivienda[i];
        }else{
            aux[i] = ipc;
        }
    }
    evolucionPrecioVivienda = aux;


    // Gastos compra
    var gastosMensualesComunidad = parseFloat($("#gastosComunidad").val())/12.0;
    var ibiMensual = parseFloat($("#ibi").val())/12.0;

    // Distintos índices e intereses
	var interesMens = loanInterest/12.0/100;
    var interesCapitalMensual = rendimientoCapital/12.0/100;

    
	// El valor de la hipoteca que hay que pedir al banco
	var gastosCompra = gastosCompraPorc/100.0 * precioCasa;
	var capital = precioCasa + gastosCompra - capitalInicial;
	cuota =  (capital * interesMens)/(1-Math.pow(1+ interesMens,-plazo));

    // Surplus. Consideramos que el presupuesto que una familia dedica a la compra
    // de la casa es el valor mayor entre alquiler y cuota de hipoteca. Ese dinero
    // se invertirá al interés indicado 
    var signo = 1;
    if(cuota < alquiler) {
        signo = -1; // La hipoteca es más barata que el alquiler
    }
    var surplus = Math.abs(cuota-alquiler);
    var accSurplus = 0.0;
	
    // Variables
	var capAmortMens = [];
	var intAmortMens = [];
    var balance = [];
	var capitalPendiente = capital;
    var balanceCompra = capitalInicial-gastosCompra;
    var totalCapitalAmortizado = 0.0;
    var balanceAlquiler = 0.0;
    var ahorrosAlquiler = capitalInicial;

    var balanceA = [];
    var balanceC = [];


    for (var i = 0; i < plazo; i++) {
        varPrecioVivienda = evolucionPrecioVivienda[Math.floor(i/12)]/12.0/100.0;
    	var interesAmortizado = capitalPendiente*interesMens;
        var capitalAmortizado = cuota -interesAmortizado;
        totalCapitalAmortizado += capitalAmortizado;
        precioCasa *= (1+varPrecioVivienda);

        // Cálculo de las cuotas
    	intAmortMens.push([i,interesAmortizado]);
    	capAmortMens.push([i,capitalAmortizado]);
    	capitalPendiente = capitalPendiente - capitalAmortizado;

        // Rendimiento del surplus
        accSurplus = accSurplus*(1+interesCapitalMensual)+surplus;

        // Cálculo del balances
        balanceCompra = precioCasa+totalCapitalAmortizado-capitalInicial-capital-gastosMensualesComunidad-ibiMensual;
        ahorrosAlquiler *= (1+interesCapitalMensual);
           
        balanceAlquiler = ahorrosAlquiler-(i+1)*alquiler;
        var balanceMensual = balanceCompra-balanceAlquiler-signo*accSurplus;
        // Lo pongo en negativo para que la opción de partida sea ver cuándo compensa comprar
        balance.push([i,-balanceMensual]);
        balanceA.push([i,balanceAlquiler]);
        balanceC.push([i,balanceCompra]);
    }


    var precioViviendaAux = [];
    for(var idx=0;idx<evolucionPrecioVivienda.length;idx++){
        precioViviendaAux.push([idx,evolucionPrecioVivienda[idx]]);
    }
    $.plot($("#balance"), [
            {
                data:balance,
                label:"Balance entre alquilar y comprar",
                lines:{show:"true",fill:"true"}
            }
            ]);

    $.plot($("#evolucionPrecioVivienda"),[
            {
                data:precioViviendaAux,
                label:"Variación anual del precio de la vivienda",
                color:"red",
                bars:{show:"true"}
            }]
    ,
            {
                grid:{clickable:"true"},
                yaxis:{min:-10,max:10,ticks:20},
                xaxis:{min:0,max:evolucionPrecioVivienda.length,ticks:20}
            });


    $.plot($("#cuotas"),[
            {
                data:capAmortMens,
                label:"Capital",
                lines:{show:true} 
            },
            {
                data:intAmortMens,
                label:"Intereses",
                lines:{show:true}
            }]);

    $.plot("#balancesSecundarios", [
            {
                data:balanceA,
                label:"balance Alquilar"
            },
            {
                data:balanceC,
                label:"balance Compra"
            }]);

    
    //return [balance, precioViviendaAux, capAmortMens, intAmortMens, balanceA, balanceC]
};

$(document).ready(function() { 
    $("#ayuda").hide();

    var balance, precioViviendaAux, capAmortMens, intAmortMens, balanceA, balanceC;

   $("#evolucionPrecioVivienda").bind("plotclick", function (event, pos, item) {
        updateEvolucionPrecioVivienda(Math.floor(pos.x),Math.round(pos.y));
        calcula();
    });

   calcula();

}); 
